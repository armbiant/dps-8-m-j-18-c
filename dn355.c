#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "j18c.h"
#include "dn355.h"

typedef uint8_t  word2;
typedef uint16_t word15;
typedef uint32_t word18;


// Memory size
enum { MEM_SIZE = 1 << 15 };

static word18 M[MEM_SIZE];
static word15 M_address;
static word18 M_data;

bool dn355_io_din (cell address, cell * data) {
  if (address == DN355_MEMORY_DATA) {
    * data = M_data & MASK18;
    return true;
  }
  return false;
}

bool dn355_io_dout (cell address, cell data) {
  if (address == DN355_MEMORY_ADDR) {
    M_address = data & MASK18;
    return true;
  }
  if (address == DN355_MEMORY_DATA) {
    M_data = data & MASK18;
    return true;
  }
  if (address == DN355_MEMORY_CTRL) {
    if (data == DN355_MEMORY_CTRL_RD) {
      M_data = M[M_address];
//printf ("rd %05o:%06o\n", M_address, M_data);
      return true;
    }
    if (data == DN355_MEMORY_CTRL_WR) {
      M[M_address] = M_data;
//printf ("wr %05o:%06o\n", M_address, M_data);
      return true;
    }
    fprintf (stderr, "bad memory ctrl %04x\n", data);
  }
  return false;
}
